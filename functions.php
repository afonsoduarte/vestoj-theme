<?php
/**
 * Vestoj functions and definitions
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Vestoj 1.0
 */

/**
 * Set up the content width value based on the theme's design.
 *
 * @see vestoj_content_width()
 *
 * @since Vestoj 1.0
 */
if ( ! isset( $content_width ) ) {
	$content_width = 474;
}

if ( ! function_exists( 'vestoj_setup' ) ) :
/**
 * Vestoj setup.
 *
 * Set up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support post thumbnails.
 *
 * @since Vestoj 1.0
 */
function vestoj_setup() {

	/*
	 * Make Vestoj available for translation.
	 *
	 * Translations can be added to the /languages/ directory.
	 * If you're building a theme based on Vestoj, use a find and
	 * replace to change 'vestoj' to the name of your theme in all
	 * template files.
	 */
	load_theme_textdomain( 'vestoj', get_template_directory() . '/languages' );

	// Add RSS feed links to <head> for posts and comments.
	add_theme_support( 'automatic-feed-links' );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary'   => __( 'Top primary menu', 'vestoj' ),
		'secondary' => __( 'Secondary menu at bottom of page', 'vestoj' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	// This theme uses its own gallery styles.
	add_filter( 'use_default_gallery_style', '__return_false' );
}
endif; // vestoj_setup
add_action( 'after_setup_theme', 'vestoj_setup' );

/**
 * Adjust content_width value for image attachment template.
 *
 * @since Vestoj 1.0
 */
function vestoj_content_width() {
	if ( is_attachment() && wp_attachment_is_image() ) {
		$GLOBALS['content_width'] = 810;
	}
}
add_action( 'template_redirect', 'vestoj_content_width' );



/**
 * Enqueue scripts and styles for the front end.
 *
 * @since Vestoj 1.0
 */
function vestoj_scripts() {
	// Load main stylesheet
	wp_enqueue_style( 'app', get_template_directory_uri() . '/css/app.css' );

	// Load our main stylesheet.
	wp_dequeue_style( 'screen' );

	wp_enqueue_script( 'slick-js', get_template_directory_uri() . '/bower_components/slick-carousel/slick/slick.min.js', array( 'jquery' ), '20140616', true );
	wp_enqueue_script( 'vestoj-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20140616', true );
}
add_action( 'wp_enqueue_scripts', 'vestoj_scripts' );


// MCE Editor custom style
add_editor_style( 'css/editor-style.css' );


/**
 * Remove YARPP styles.
 *
 * @since Vestoj 1.0
 */

add_action('wp_print_styles','lm_dequeue_header_styles');
function lm_dequeue_header_styles()
{
  wp_dequeue_style('yarppWidgetCss');
}

add_action('get_footer','lm_dequeue_footer_styles');
function lm_dequeue_footer_styles()
{
  wp_dequeue_style('yarppRelatedCss');
}


if ( ! function_exists( 'vestoj_the_attached_image' ) ) :
/**
 * Print the attached image with a link to the next attached image.
 *
 * @since Vestoj 1.0
 */
function vestoj_the_attached_image() {
	$post                = get_post();
	/**
	 * Filter the default Vestoj attachment size.
	 *
	 * @since Vestoj 1.0
	 *
	 * @param array $dimensions {
	 *     An array of height and width dimensions.
	 *
	 *     @type int $height Height of the image in pixels. Default 810.
	 *     @type int $width  Width of the image in pixels. Default 810.
	 * }
	 */
	$attachment_size     = apply_filters( 'vestoj_attachment_size', array( 810, 810 ) );
	$next_attachment_url = wp_get_attachment_url();

	/*
	 * Grab the IDs of all the image attachments in a gallery so we can get the URL
	 * of the next adjacent image in a gallery, or the first image (if we're
	 * looking at the last image in a gallery), or, in a gallery of one, just the
	 * link to that image file.
	 */
	$attachment_ids = get_posts( array(
		'post_parent'    => $post->post_parent,
		'fields'         => 'ids',
		'numberposts'    => -1,
		'post_status'    => 'inherit',
		'post_type'      => 'attachment',
		'post_mime_type' => 'image',
		'order'          => 'ASC',
		'orderby'        => 'menu_order ID',
	) );

	// If there is more than 1 attachment in a gallery...
	if ( count( $attachment_ids ) > 1 ) {
		foreach ( $attachment_ids as $attachment_id ) {
			if ( $attachment_id == $post->ID ) {
				$next_id = current( $attachment_ids );
				break;
			}
		}

		// get the URL of the next image attachment...
		if ( $next_id ) {
			$next_attachment_url = get_attachment_link( $next_id );
		}

		// or get the URL of the first image attachment.
		else {
			$next_attachment_url = get_attachment_link( array_shift( $attachment_ids ) );
		}
	}

	printf( '<a href="%1$s" rel="attachment">%2$s</a>',
		esc_url( $next_attachment_url ),
		wp_get_attachment_image( $post->ID, $attachment_size )
	);
}
endif;



/**
 * Extend the default WordPress body classes.
 *
 * Adds body classes to denote:
 * 1. Single or multiple authors.
 * 2. Presence of header image except in Multisite signup and activate pages.
 * 3. Index views.
 * 4. Full-width content layout.
 * 5. Presence of footer widgets.
 * 6. Single views.
 * 7. Featured content layout.
 *
 * @since Vestoj 1.0
 *
 * @param array $classes A list of existing body class values.
 * @return array The filtered body class list.
 */
function vestoj_body_classes( $classes ) {
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	if ( get_header_image() ) {
		$classes[] = 'header-image';
	} elseif ( ! in_array( $GLOBALS['pagenow'], array( 'wp-activate.php', 'wp-signup.php' ) ) ) {
		$classes[] = 'masthead-fixed';
	}

	if ( is_archive() || is_search() || is_home() ) {
		$classes[] = 'list-view';
	}

	if ( ( ! is_active_sidebar( 'sidebar-2' ) )
		|| is_page_template( 'page-templates/full-width.php' )
		|| is_page_template( 'page-templates/contributors.php' )
		|| is_attachment() ) {
		$classes[] = 'full-width';
	}

	if ( is_active_sidebar( 'sidebar-3' ) ) {
		$classes[] = 'footer-widgets';
	}

	if ( is_singular() && ! is_front_page() ) {
		$classes[] = 'singular';
	}

	return $classes;
}
add_filter( 'body_class', 'vestoj_body_classes' );

/**
 * Extend the default WordPress post classes.
 *
 * Adds a post class to denote:
 * Non-password protected page with a post thumbnail.
 *
 * @since Vestoj 1.0
 *
 * @param array $classes A list of existing post class values.
 * @return array The filtered post class list.
 */
function vestoj_post_classes( $classes ) {
	if ( ! post_password_required() && ! is_attachment() && has_post_thumbnail() ) {
		$classes[] = 'has-post-thumbnail';
	}

	return $classes;
}
add_filter( 'post_class', 'vestoj_post_classes' );

/**
 * Create a nicely formatted and more specific title element text for output
 * in head of document, based on current view.
 *
 * @since Vestoj 1.0
 *
 * @global int $paged WordPress archive pagination page count.
 * @global int $page  WordPress paginated post page count.
 *
 * @param string $title Default title text for current view.
 * @param string $sep Optional separator.
 * @return string The filtered title.
 */
function vestoj_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() ) {
		return $title;
	}

	// Add the site name.
	$title .= get_bloginfo( 'name', 'display' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) ) {
		$title = "$title $sep $site_description";
	}

	// Add a page number if necessary.
	if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
		$title = "$title $sep " . sprintf( __( 'Page %s', 'vestoj' ), max( $paged, $page ) );
	}

	return $title;
}
add_filter( 'wp_title', 'vestoj_wp_title', 10, 2 );

// Custom template tags for this theme.
require get_template_directory() . '/app/template-tags.php';

/*
 * Setup
 *
 */

require get_template_directory() . '/app/config/post-types.php';
require get_template_directory() . '/app/config/image-sizes.php';
require get_template_directory() . '/app/config/tiny-mce.php';
require get_template_directory() . '/app/config/advanced-custom-fields.php';

/*
 * Models
 *
 */
require get_template_directory() . '/app/models/archive.php';
require get_template_directory() . '/app/models/contributors.php';
require get_template_directory() . '/app/models/excerpt.php';
require get_template_directory() . '/app/models/gallery.php';
require get_template_directory() . '/app/models/index.php';
require get_template_directory() . '/app/models/issues.php';
require get_template_directory() . '/app/models/keywords.php';

