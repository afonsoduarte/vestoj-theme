<?php 

/*
 * Model for home page
 *
 */

function vestoj_home_filter( $query ) {
  if ( $query->is_home() && $query->is_main_query() ) {
    $query->set( 'posts_per_page', '7' );
  }
}
add_action( 'pre_get_posts', 'vestoj_home_filter' );
