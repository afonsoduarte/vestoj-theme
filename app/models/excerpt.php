<?php 

/*
 * Model for excerpts
 *
 */


// Check if this is first post
function vestoj_is_first() {
  global $wp_query;
  return ($wp_query->current_post === 0 && !is_paged() && is_home() );
}

// Return post classes
function vestoj_excerpt_classes() {
  $classes = array();
  $classes[] = 'hentry-excerpt';

  $pos = vestoj_is_first()? 'featured' : 'small';
  $classes[] = 'excerpt-'.$pos;
  return $classes;
}

// Excerpt Thumbnails
function vestoj_the_post_tb_src() {
  global $post;
  $image_size = vestoj_is_first()? 'featured' : 'post-thumbnail';
  $tb_ID = get_post_thumbnail_id( $post->ID );
  $image_attributes = wp_get_attachment_image_src( $tb_ID, $image_size );
  echo $image_attributes[0];
}

// Read more link
function vestoj_excerpt_more( $more ) {
  return ' <a class="read-more" href="'. get_permalink( get_the_ID() ) . '">' . __('Read More', 'vestoj') . '</a>';
}
add_filter( 'excerpt_more', 'vestoj_excerpt_more' );
