<?php 

/*
 * Model for back issues
 *
 */

function vestoj_get_issues() {
  $poster_args = array(
    'post_type' => 'vestoj_issues',
    'posts_per_page' => -1, 
    'orderby' => 'date',
    'order' => 'DESC'
   );
  return get_posts($poster_args); 
}


// Issue Thumbnails
function vestoj_the_issue_tb() {
  global $post;
  $image_size = 'issue-thumbnail';
  $tb_ID = get_post_thumbnail_id( $post->ID );
  $image_attributes = wp_get_attachment_image_src( $tb_ID, $image_size );
  echo $image_attributes[0];
}
