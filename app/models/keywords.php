<?php 

if ( ! function_exists( 'vestoj_list_tags' ) ) :
/**
 * Print a list of all tags, listed alphabetically.
 *
 * @since Vestoj 1.0
 */
function vestoj_list_tags() {
  $args = array(
    'orderby'           => 'slug', 
    'order'             => 'ASC',
    'hide_empty'        => true
  ); 
  $tags = get_tags( array('orderby' => 'name', 'order' => 'ASC') );

  return $tags;
}
endif;