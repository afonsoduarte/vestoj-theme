<?php 

if ( ! function_exists( 'vestoj_list_authors' ) ) :
/**
 * Print a list of all site contributors who published at least one post.
 *
 * @since Vestoj 1.0
 */
function vestoj_list_authors() {
  $contributor_ids = get_users( array(
    'fields'  => 'ID',
    'orderby' => 'name',
    'order'   => 'ASC',
    'role'    => 'contributor'
  ) );

  return $contributor_ids;
}
endif;