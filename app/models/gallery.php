<?php 

/*
 * Model for gallery
 *
 */

function vestoj_gallery_atts( $out, $pairs, $atts ) {
   
    $atts = shortcode_atts( array(
        'columns' => '2',
        'size' => 'large',
         ), $atts );

    $out['columns'] = $atts['columns'];
    $out['size'] = $atts['size'];

    return $out;
}
add_filter( 'shortcode_atts_gallery', 'vestoj_gallery_atts', 10, 3 );



// Register the html5 figure-non-responsive code fix. 
function vestoj_img_caption_shortcode( $dummy, $attr, $content ){

  // To do: Add markdown to image captions

  $atts = shortcode_atts( array(
      'id'      => '',
      'align'   => 'alignnone',
      'width'   => '',
      'caption' => '',
      'class'   => '',
  ), $attr, 'caption' );

  $atts['width'] = (int) $atts['width'];
  if ( $atts['width'] < 1 || empty( $atts['caption'] ) )
      return $content;

  if ( ! empty( $atts['id'] ) )
      $atts['id'] = 'id="' . esc_attr( $atts['id'] ) . '" ';

  $class = trim( 'wp-caption ' . $atts['align'] . ' ' . $atts['class'] );

  if ( current_theme_supports( 'html5', 'caption' ) ) {
      return '<figure ' . $atts['id'] . 'style="xmax-width: ' . (int) $atts['width'] . 'px;" class="' . esc_attr( $class ) . '">'
      . do_shortcode( $content ) . '<figcaption class="wp-caption-text">' . $atts['caption'] . '</figcaption></figure>';
  }

  // Return nothing to allow for default behaviour!!!
  return '';

}

add_filter( 'img_caption_shortcode', 'vestoj_img_caption_shortcode', 10, 3 );
