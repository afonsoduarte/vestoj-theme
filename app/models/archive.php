<?php 

/*
 * Model for archive pages
 *
 */

function vestoj_archive_filter( $query ) {
  if ( $query->is_archive() && $query->is_main_query() ) {
    $query->set( 'posts_per_page', '12' );
  }
}
add_action( 'pre_get_posts', 'vestoj_archive_filter' );
