<?php

// POST TYPES
add_action( 'init', 'create_post_type' );

function create_post_type() {

  register_post_type( 'vestoj_news',
    array(
      'labels' => array(
        'name' => __( 'News' ),
        'singular_name' => __( 'News' )
      ),
    'menu_position' => 2,
    'hierarchical' => false,
    'public' => true,
    'has_archive' => true,
    'show_ui'=> true,
    'exclude_from_search'=> false,
    'supports'=>array('title', 'editor', 'custom-fields', 'page-attributes', 'thumbnail'),
    'rewrite' => array('slug' => 'news', 'with_front' => false)
    )
  );

  register_post_type( 'vestoj_issues',
    array(
      'labels' => array(
        'name' => __( 'Issues' ),
        'singular_name' => __( 'Issue' )
      ),
    'menu_position' => 3,
    'hierarchical' => false,
    'public' => true,
    'has_archive' => true,
    'show_ui'=> true,
    'exclude_from_search'=> false,
    'supports'=>array('title', 'editor', 'custom-fields', 'page-attributes', 'thumbnail'),
    'rewrite' => array('slug' => 'issues', 'with_front' => false)
    )
  );
}