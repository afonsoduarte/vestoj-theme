<?php
function vestoj_image_sizes() {

    add_theme_support('post-thumbnails');
    set_post_thumbnail_size( 380, 999, false );

    // New image sizes
    add_image_size('featured', 1180, 9999); 
    add_image_size('issue-thumbnail', 200, 265, 0); 

}

add_action('after_setup_theme', 'vestoj_image_sizes'); 

?>