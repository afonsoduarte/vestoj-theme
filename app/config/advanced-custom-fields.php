<?php 

if(function_exists("register_field_group"))
{
  register_field_group(array (
    'id' => 'acf_posts',
    'title' => 'Posts',
    'fields' => array (
      array (
        'key' => 'field_53fb5b14e88dc',
        'label' => 'Sub Title',
        'name' => 'sub_title',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'none',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_54975040a7a72',
        'label' => 'Image contributor',
        'name' => 'image_contributor',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
    ),
    'location' => array (
      array (
        array (
          'param' => 'post_type',
          'operator' => '==',
          'value' => 'post',
          'order_no' => 0,
          'group_no' => 0,
        ),
      ),
    ),
    'options' => array (
      'position' => 'acf_after_title',
      'layout' => 'no_box',
      'hide_on_screen' => array (
        0 => 'permalink',
        1 => 'custom_fields',
        2 => 'discussion',
        3 => 'comments',
        4 => 'revisions',
        5 => 'slug',
        6 => 'format',
        7 => 'send-trackbacks',
      ),
    ),
    'menu_order' => 0,
  ));
}

