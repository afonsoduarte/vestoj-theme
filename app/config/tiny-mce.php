<?php 

/*
 * 
 * CUSTOM TINY MCE STYLES
 *
 */

// Add Formats Dropdown Menu To MCE
if ( ! function_exists( 'vestoj_style_select' ) ) {
  function vestoj_style_select( $buttons ) {
    array_push( $buttons, 'styleselect' );
    return $buttons;
  }
}
add_filter( 'mce_buttons', 'vestoj_style_select' );


// Add new styles to the TinyMCE "formats" menu dropdown
if ( ! function_exists( 'vestoj_styles_dropdown' ) ) {
  function vestoj_styles_dropdown( $settings ) {

    // Create array of new styles
    $new_styles = array(
      array(
        'title'   => __('Quote','vestoj'),
        'classes' => 'quote',
        'block' => 'blockquote', 
        'wrapper' => true
      ),
      array(
        'title'   => __('Quote reference','vestoj'),
        'block'  => 'cite',
        'classes' => 'cite',
        'wrapper' => false
      ),
      array(
        'title'   => __('Intro text','vestoj'),
        'block'  => 'p',
        'classes' => 'intro-text',
        'wrapper' => false
      ),
      array(
        'title'   => __('Question','vestoj'),
        'block'  => 'p',
        'classes' => 'question',
        'wrapper' => false
      )
    );

    // Merge old & new styles
    // $settings['style_formats_merge'] = true;

    // Add new styles
    $settings['style_formats'] = json_encode( $new_styles );

    // Return New Settings
    return $settings;

  }
}
add_filter( 'tiny_mce_before_init', 'vestoj_styles_dropdown' );