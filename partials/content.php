<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('hentry-full'); ?>>

  <header class="entry-header">

    <p class="entry-category"><?php the_category(' '); ?></p>

    <h1 class="entry-title">
      <?php the_title(); ?>
    </h1>

    <p class="entry-sub-title">
      <?php echo get_metadata( 'post', get_the_ID(), 'sub_title', true ); ?>
    </p>

    <div class="entry-meta">
      <?php
        if ( 'post' == get_post_type() )
          vestoj_posted_on();
      ?>
      <?php if($image_contributor = get_metadata( 'post', get_the_ID(), 'image_contributor', true )): ?>
      <p class="entry-image-contributor">
        <?php echo $image_contributor ?>
      </p>
      <?php endif; ?>
    </div><!-- .entry-meta -->

    <?php the_tags( '<div class="entry-tags"><span class="tag-title">Keywords:</span> ', ', ', '</div>' ); ?>
  </header><!-- .entry-header -->

  <?php if ( is_search() && !is_single() ) : ?>
  <div class="entry-summary">
    <?php the_excerpt(); ?>
  </div><!-- .entry-summary -->
  <?php else : ?>
  <div class="entry-content">
    <?php the_content(); ?>
  </div><!-- .entry-content -->
  <?php endif; ?>

</article><!-- #post-## -->
