<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('excerpt-issue' ); ?>>

  <a href="<?php the_permalink(); ?>">
    <img src="<?php vestoj_the_issue_tb(); ?>">
  </a>

  <h1 class="issue-title screen-reader-text"><?php the_title(); ?></h1>
  
</article><!-- #post-## -->
