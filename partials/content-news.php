<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('hentry-full'); ?>>

  <header class="entry-header">

    <p class="pubdate">
      <time datetime="<?php echo esc_attr( get_the_date( 'c' ) ); ?>">  <?php echo esc_html( get_the_date() ); ?>
      </time>
    </p>

    <h1 class="entry-title">
      <?php the_title(); ?>
    </h1>

    <div class="entry-meta">
      <?php
        if ( 'post' == get_post_type() )
          vestoj_posted_on();
      ?>
    </div><!-- .entry-meta -->

    <?php the_tags( '<div class="entry-tags"><span class="tag-title">Keywords:</span> ', ', ', '</div>' ); ?>
  </header><!-- .entry-header -->

  <?php if ( is_search() && !is_single() ) : ?>
  <div class="entry-summary">
    <?php the_excerpt(); ?>
  </div><!-- .entry-summary -->
  <?php else : ?>
  <div class="entry-content">
    <?php the_content(); ?>
  </div><!-- .entry-content -->
  <?php endif; ?>

</article><!-- #post-## -->
