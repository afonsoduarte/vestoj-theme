<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(vestoj_excerpt_classes() ); ?>>

  <header class="entry-header">

    <a class="tb" 
       style="background-image: url(<?php vestoj_the_post_tb_src(); ?> );"
       href="<?php the_permalink(); ?>" rel="bookmark"
       title="<?php the_title(); ?>">
       <?php the_title(); ?>
    </a>

    <p class="entry-category"><?php the_category(' '); ?></p>

    <h1 class="entry-title">
      <a href="<?php the_permalink(); ?>" rel="bookmark">
        <?php the_title(); ?>
      </a>
    </h1>

    <p class="entry-sub-title">
      <a href="<?php the_permalink(); ?>" rel="bookmark">
      <?php echo get_metadata( 'post', get_the_ID(), 'sub_title', true ); ?>
      </a>
    </p>

    <div class="entry-meta">
      <?php
        if ( 'post' == get_post_type() )
          vestoj_posted_on();
      ?>
    </div><!-- .entry-meta -->
  </header><!-- .entry-header -->

  <div class="entry-summary">
    <?php the_excerpt(); ?>
    <p class="read-more">
      <a href="<?php the_permalink(); ?>" rel="bookmark">READ MORE</a>
    </p>
  </div><!-- .entry-summary -->
  
</article><!-- #post-## -->
