<?php 
/*
YARPP Template: Simple
Author: Afonso
Description: A simple YARPP template.
*/
?>
<?php if (have_posts()):?>
<section class="related">
  <h3 class="footer-title">Related articles from Vestoj</h3>
  <div class="small-post-grid">
  	<?php while (have_posts()) : the_post(); ?>
      <?php get_template_part( 'partials/excerpt' ); ?>
  	<?php endwhile; ?>
  </div>
</section>
<?php endif; ?>
