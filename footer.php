<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>
		<aside class="back-issues">
		  <h1 class="section-title">Back Issues</h1>
		  <?php $issues = vestoj_get_issues(); ?>
		  <?php if($issues): ?>
		  <section id="issues">
		    <?php foreach ($issues as $post): setup_postdata( $post ); ?>
		      <?php get_template_part( 'partials/excerpt', 'issue' ); ?>
		    <?php endforeach; wp_reset_postdata(); ?>
		  </section>
		  <?php endif;?>
		</aside>

		</div><!-- #main -->

		<footer id="colophon" class="site-footer" role="contentinfo">
			<header class="footer-header">
				<h1 class="site-title-footer"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<p class="site-tag-line"><?php bloginfo( 'description' ); ?></p>
			</header>

			<?php wp_nav_menu( array( 'theme_location' => 'secondary', 'menu_class' => 'bottom-menu' ) ); ?>
			<div class="site-info">
				
			</div><!-- .site-info -->
		</footer><!-- #colophon -->
	</div><!-- #page -->

	<?php wp_footer(); ?>
</body>
</html>