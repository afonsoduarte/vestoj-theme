<?php
/**
 * Template Name: Keywords Page
 *
 * @package WordPress
 * @subpackage Vestoj
 * @since Vestoj 1.0
 */

get_header(); ?>

<div id="main-content" class="main-content">

  <div id="primary" class="content-area">
    <div id="contmeent" class="site-content" role="main">
      <?php
        // Start the Loop.
        while ( have_posts() ) : the_post();
      ?>

      <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        
        <?php the_title( '<header class="archive-header"><h1 class="archive-title">', '</h1></header><!-- .entry-header -->' ); ?>

        <div class="intro-text">
          <?php the_content(); ?>
        </div>

        <div class="post-grid">
          <div class="keywords">
        <?php 
          // Output the authors list.
          $keywords = vestoj_list_tags();
          $first_letter = "";

          foreach ( $keywords as $key => $keyword ) : ?>
          <?php 
            // Alphabetical separators.
            if ( $first_letter != substr($keyword->slug, 0,1) ): 
              $first_letter = substr($keyword->slug, 0,1); 
          ?>
            </div><!-- .keywords -->
            <h1 class="alphabetical-separator">
              <?php echo $first_letter; ?>
            </h1>
            <div class="keywords">
            <?php endif; ?>
              <div class="Xhentry-excerpt keyword">
                <h2 class="keyword-name">
                  <a class="keyword-page-link" href="<?php echo get_tag_link($keyword->term_id); ?>">
                    <?php echo $keyword->name; ?>
                  </a>
                </h2>
              </div><!-- .keyword -->
          <?php endforeach; ?>
          </div><!-- .keywords -->
        </div><!-- .post-list -->
      </article><!-- #post-## -->

      <?php
        endwhile;
      ?>
    </div><!-- #content -->
  </div><!-- #primary -->
</div><!-- #main-content -->

<?php
get_footer();
