<?php
/**
 * Template Name: Contributor Page
 *
 * @package WordPress
 * @subpackage Vestoj
 * @since Vestoj 1.0
 */

get_header(); ?>

<div id="main-content" class="main-content">

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
			<?php
				// Start the Loop.
				while ( have_posts() ) : the_post();
			?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				
				<?php the_title( '<header class="archive-header"><h1 class="archive-title">', '</h1></header><!-- .entry-header -->' ); ?>

				<div class="intro-text">
					<?php the_content(); ?>
				</div>

				<div class="post-list">
				<?php 
					// Output the authors list.
					$contributor_ids = vestoj_list_authors();

					foreach ( $contributor_ids as $contributor_id ) :
					  $post_count = count_user_posts( $contributor_id );

					  // Move on if user has not published a post (yet).
					  if ( ! $post_count ) {
					    continue;
					  }
					?>
					<div class="contributor">
			      <h2 class="contributor-name">
			        <a class="contributor-posts-link" href="<?php echo esc_url( get_author_posts_url( $contributor_id ) ); ?>">
			          <?php echo get_the_author_meta( 'display_name', $contributor_id ); ?>
			        </a>
			      </h2>
					</div><!-- .contributor -->
					<?php endforeach; ?>
				</div><!-- .post-list -->
			</article><!-- #post-## -->

			<?php
				endwhile;
			?>
		</div><!-- #content -->
	</div><!-- #primary -->
</div><!-- #main-content -->

<?php
get_footer();
