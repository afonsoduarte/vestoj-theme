<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

  <div id="primary" class="content-area">
    <div id="content" class="site-content" role="main">
      <?php
        // Start the Loop.
        while ( have_posts() ) : the_post();

          get_template_part( 'partials/content' );

        endwhile;
      ?>
    </div><!-- #content -->
  </div><!-- #primary -->

  <?php if( get_post_type() == 'post' ): ?>
  <footer class="entry-footer">
    <section class="author-bio">
      <h1 class="footer-title">By <?php the_author(); ?></h1>
      <?php the_author_meta('description'); ?>
      <p class="author-link">
        <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>">Read other articles by <?php the_author(); ?></a>
        </p>
    </section>

    <?php related_posts(); ?>
  </footer>
  <?php endif; ?>

<?php
get_footer();
